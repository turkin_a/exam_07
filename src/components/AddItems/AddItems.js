import React from 'react';
import './AddItems.css';
import Food from "../CardOfGood/Food/Food";
import Drink from "../CardOfGood/Drink/Drink";

const AddItems = props => {
  let addFood = (items) => {
    return Object.keys(items).map((item) => {
      return <Food
        item={item}
        price={props.price[item]}
        click={props.click}
        key={item}
      />
    });
  };

  let addDrink = (items) => {
    return Object.keys(items).map((item) => {
      return <Drink
        item={item}
        price={props.price[item]}
        click={props.click}
        key={item}
      />
    });
  };

  return (
    <div className="AddItems">
      <span className="Title">Add items:</span>
      <div className="FoodItems">
        {addFood(props.goods.food)}
      </div>
      <div className="DrinkItems">
        {addDrink(props.goods.drink)}
      </div>
    </div>
  );
};

export default AddItems;