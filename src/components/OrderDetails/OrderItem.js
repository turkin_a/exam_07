import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
  return (
    <div className="OrderItem">
      <span className="TitleOfOrder">{props.item}</span>
      <i className="RemoveOfOrder fa fa-times" aria-hidden="true" title="Remove this item" onClick={() => props.click(props.item)}> </i>
      <span className="PriceOfOrder">{props.price} KGS</span>
      <span className="ValueOfOrder">x {props.value}</span>
    </div>
  );
};

export default OrderItem;