import React from 'react';
import './OrderDetails.css';
import OrderItem from "./OrderItem";

const OrderDetails = props => {
  let total = '';

  let addItem = (items) => {
    return Object.keys(items).map((item) => {
      if (items[item].length > 0) {
        return <OrderItem
          item={item}
          price={props.price[item]}
          value={items[item].length}
          click={props.click}
          key={item}
        />
      }
      else return null;
    });
  };

  if (props.sum === 0) total = 'Order is empty!';
  else total = `Total price: ${props.sum} KGS`;

  return (
    <div className="OrderDetails">
      <span className="Title">Order details:</span>
      {addItem(props.goods.food)}
      {addItem(props.goods.drink)}
      <div className="Total">{total}</div>
    </div>
  );
};

export default OrderDetails;