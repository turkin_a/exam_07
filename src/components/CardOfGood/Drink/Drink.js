import React from 'react';
import './Drink.css';

const Drink = props => {
  return (
    <div className="Drink" onClick={() => props.click(props.item)}>
      <i className="IconOfGood fa fa-coffee" aria-hidden="true"> </i>
      <div className="TitleOfGood">{props.item}</div>
      <div className="PriceOfGood">Price: {props.price} KGS</div>
    </div>
  );
};

export default Drink;