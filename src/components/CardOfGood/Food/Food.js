import React from 'react';
import './Food.css';

const Food = props => {
  return (
    <div className="Food" onClick={() => props.click(props.item)}>
      <i className="IconOfGood fa fa-cutlery" aria-hidden="true"> </i>
      <div className="TitleOfGood">{props.item}</div>
      <div className="PriceOfGood">Price: {props.price} KGS</div>
    </div>
  );
};

export default Food;