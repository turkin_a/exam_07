import React, { Component } from 'react';
import './App.css';
import OrderDetails from "../components/OrderDetails/OrderDetails";
import AddItems from "../components/AddItems/AddItems";

const price = {
  Hamburger: 80,
  Cheeseburger: 90,
  Fries: 45,
  Coffee: 70,
  Tea: 50,
  Cola: 40
};

class App extends Component {
  state = {
      food: {
        Hamburger: [],
        Cheeseburger: [],
        Fries: []
      },
      drink: {
        Coffee: [],
        Tea: [],
        Cola: []
      }
  };

  calculateOrderSum = () => {
    let sum = 0;

    for (let item in this.state.food) {
      sum += this.state.food[item].reduce((acc, el) => {return acc + el;}, 0);
    }

    for (let item in this.state.drink) {
      sum += this.state.drink[item].reduce((acc, el) => {return acc + el;}, 0);
    }

    return sum;
  };

  addGoodToOrder = (item) => {
    let food = {...this.state.food};
    let drink = {...this.state.drink};

    if (food.hasOwnProperty(item)) {
      let productItem = [...food[item]];
      productItem.push(price[item]);
      food[item] = productItem;
    }

    if (drink.hasOwnProperty(item)) {
      let productItem = [...drink[item]];
      productItem.push(price[item]);
      drink[item] = productItem;
    }

    this.setState({food, drink});
  };

  removeGoodFromOrder = (item) => {
    let food = {...this.state.food};
    let drink = {...this.state.drink};

    if (food.hasOwnProperty(item)) {
      let productItem = [...food[item]];
      productItem.splice(0, productItem.length);
      food[item] = productItem;
    }

    if (drink.hasOwnProperty(item)) {
      let productItem = [...drink[item]];
      productItem.splice(0, productItem.length);
      drink[item] = productItem;
    }

    this.setState({food, drink});
  };

  render() {
    return (
      <div className="App">
        <OrderDetails
          goods={this.state}
          price={price}
          sum={this.calculateOrderSum()}
          click={this.removeGoodFromOrder}
        />
        <AddItems
          goods={this.state}
          price={price}
          click={this.addGoodToOrder}
        />
      </div>
    );
  }
}

export default App;
